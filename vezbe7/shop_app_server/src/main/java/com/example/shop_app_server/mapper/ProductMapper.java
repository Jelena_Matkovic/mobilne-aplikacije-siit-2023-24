package com.example.shop_app_server.mapper;

import com.example.shop_app_server.dto.ProductDto;
import com.example.shop_app_server.model.Product;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class ProductMapper {

    public Product getEntityFromDto(ProductDto dto){
        return Product.builder()
                .id(dto.getId())
                .title(dto.getTitle())
                .description(dto.getDescription())
                .image(dto.getImagePath())
                .build();
    }
    public List<Product> getListEntityFromListDto(List<ProductDto> productDtos){
        List<Product> products = new ArrayList<>();
        for(ProductDto productDto : productDtos){
            products.add(getEntityFromDto(productDto));
        }
        return products;
    }
    public ProductDto getDtoFromEntity(Product product){
        return ProductDto.builder()
                .id(product.getId())
                .title(product.getTitle())
                .description(product.getDescription())
                .imagePath(product.getImage())
                .build();
    }

    public List<ProductDto> getListDtoFromListEntity(List<Product> products){
        List<ProductDto> productDtos = new ArrayList<>();
        for(Product product : products){
            productDtos.add(getDtoFromEntity(product));
        }
        return productDtos;
    }

}
