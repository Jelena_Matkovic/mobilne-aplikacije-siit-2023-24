package com.example.shop_app_server.service;

import com.example.shop_app_server.dto.ProductDto;
import com.example.shop_app_server.mapper.ProductMapper;
import com.example.shop_app_server.model.Product;
import com.example.shop_app_server.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    public ProductDto getById(Long id){
        return productMapper.getDtoFromEntity(productRepository.getById(id));
    }

    public List<ProductDto> getAll(){
        return productMapper.getListDtoFromListEntity(productRepository.getAll());
    }

    public ProductDto add(ProductDto productDto){
        Product product = productRepository.add(productMapper.getEntityFromDto(productDto));
        return productMapper.getDtoFromEntity(product);
    }
    public Boolean deleteById(Long id){
        return productRepository.deleteById(id);
    }

    public ProductDto edit(ProductDto productDto){
        return productMapper.getDtoFromEntity(productRepository.edit(productMapper.getEntityFromDto(productDto)));
    }

}
