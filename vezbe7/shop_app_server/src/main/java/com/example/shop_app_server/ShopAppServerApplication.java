package com.example.shop_app_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopAppServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopAppServerApplication.class, args);
	}

}
