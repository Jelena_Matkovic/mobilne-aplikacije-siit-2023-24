# Mobilne aplikacije (2023/2024)

Materijali za vežbe na predmetu **Mobilne aplikacije (MA)** na smeru **Softversko inženjerstvo i informacione tehnologije (SIIT)**, Fakultet tehničkih nauka, Novi Sad. 

[![Ftn logo](ftn-logo.png)](http://www.ftn.uns.ac.rs/691618389/fakultet-tehnickih-nauka)


## Termini  
- [Vežbe 1 - Uvod](https://gitlab.com/Jelena_Matkovic/mobilne-aplikacije-siit-2023-24/-/tree/main/vezbe1?ref_type=heads)  
- [Vežbe 2 - Aktivnosti i fragmenti](https://gitlab.com/Jelena_Matkovic/mobilne-aplikacije-siit-2023-24/-/tree/main/vezbe2?ref_type=heads)
- [Vežbe 3 - GUI I](https://gitlab.com/Jelena_Matkovic/mobilne-aplikacije-siit-2023-24/-/tree/main/vezbe3)